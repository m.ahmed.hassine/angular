import { Component } from '@angular/core';
import { MessageService } from './services/message.service';
import { PanierService } from './services/panier.service';

@Component({
  selector: 'app-root',                // nom de l'élément dans le DOM
  templateUrl: './app.component.html', // chemin de la vue
  styleUrls: ['./app.component.css']   // chemin des css associées
})
export class AppComponent {
  // attribut :
  title = "Angular c'est super";

  constructor(public messageService : MessageService,
              public panierService : PanierService){}


}
