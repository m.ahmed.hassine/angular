import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DogsComponent } from './components/dogs/dogs.component';
import { DogDetailComponent } from './components/dog-detail/dog-detail.component';
import { PlopComponent } from './components/Plop/plop.component';
import { PanierDetailsComponent } from './components/panier-details/panier-details.component';
import { AppRoutingModule } from './app-routing.module';
import { MovieSearchComponent } from './components/movie-search/movie-search.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';

@NgModule({
   declarations: [
      AppComponent,
      DogsComponent,
      DogDetailComponent,
      PlopComponent,
      PanierDetailsComponent,
      MovieSearchComponent,
      MovieDetailComponent
   ],
   imports: [
      BrowserModule,
      FormsModule,
      AppRoutingModule,
      HttpClientModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
