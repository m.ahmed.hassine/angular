import { Injectable } from '@angular/core';
import { Dog } from '../entities/dog';
import { DOGS } from '../mocks/mock-dogs';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class DogService {

getDogs() : Observable<Dog[]>{
  this.messageService.add("Va chercher les chiens !");
  return of(DOGS);
}

getDogById(id:number) : Observable<Dog>{
  let result : Dog

  for (const dog of DOGS) {
    if (dog.id == id) {
      result = dog;
    }
  }

  return of(result);
}

constructor(private messageService : MessageService) { }

}
