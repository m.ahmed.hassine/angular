import { Component, OnInit } from '@angular/core';
import { Dog } from 'src/app/entities/dog';
import { DogService } from 'src/app/services/dog.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fiche-dog',
  templateUrl: './fiche-dog.component.html',
  styleUrls: ['./fiche-dog.component.css']
})
export class FicheDogComponent implements OnInit {

  dog : Dog;

  constructor(private svc : DogService,
              private route : ActivatedRoute
            ) { }

  ngOnInit() {

    // récupérer l'idDuChien dans l'url (route)
    let id = Number(this.route.snapshot.paramMap.get('idDuChien'));

    // récupération de l'objet chien aupres du service :
    this.svc.getDogById(id)
            .subscribe(d => this.dog = d);
  }

}
