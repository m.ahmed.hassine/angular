import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/entities/movie';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css']
})
export class MovieSearchComponent implements OnInit {

  movies : Movie[];
  titre : string;

  constructor(private movieService : MoviesService) { }

  chercher()
  {
    this.movieService.searchMovies(this.titre).subscribe((data:any) => {this.movies = data.Search;});
  }

  ngOnInit() {
  }

}
