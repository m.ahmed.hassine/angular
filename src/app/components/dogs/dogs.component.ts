import { Component, OnInit } from '@angular/core';
import { Dog } from '../../entities/dog';
import { DogService } from 'src/app/services/dog.service';
import { MessageService } from 'src/app/services/message.service';
import { MoviesService } from 'src/app/services/movies.service';
import { Movie } from 'src/app/entities/movie';

@Component({
  selector: 'app-dogs',
  templateUrl: './dogs.component.html',
  styleUrls: ['./dogs.component.css']
})
export class DogsComponent implements OnInit {

  dogs : Dog[];
  selectedDog : Dog;

  movie : Movie;

  constructor(private dogService : DogService,
              public messageService : MessageService,
              private movieService : MoviesService) { }

  ngOnInit(): void {
    this.dogService.getDogs()
        .subscribe(result => this.dogs = result);

    this.movieService.getMovie('ben')
        .subscribe(data => this.movie = data);
  }

  choisir(dog : Dog) : void {
    let message = "Chien sélectionné : " + dog.nom;
    this.messageService.add(message);
    this.selectedDog = dog;
  }

}
