import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/entities/movie';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  movie:Movie;

  constructor(private route : ActivatedRoute,
              private movieService:MoviesService) { }


  genres()
  {
    return this.movie.Genre.split(',');

  }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');

    this.movieService.getMovieById(id).subscribe(m => this.movie = m);
  }

}
