export class Movie {
    
    imdbID:string;
    Title : string;
    Year : number;
    Rated : string;
    Released: string;
    Runtime: string;
    Genre: string;
    Director: string;
    Writer:  string;
    Actors:  string;
    Plot:  string;
    Language: string;
    Country: string;
    Awards: string;
    Poster: string;

    
    genres : string[] = this.Genre.split(','); 
    
}
